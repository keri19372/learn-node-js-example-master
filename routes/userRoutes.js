const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    let users = UserService.getAllUsers();
    // TODO: check if empty
    res.status(200).send(users);
}, responseMiddleware);

router.get('/:value', (req, res, next) => {
    let user = UserService.find(req.params.value)
    // TODO: check if empty
    if ( typeof user !== 'undefined') {
        res.status(200).send(user);
    } else {
    res.status(404).send({error: true,message: "Can't find user"});
    }

}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    const newUser = ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        password: req.body.password
    });
    let user = UserService.add(newUser);
    res.status(200).send(user)
}, responseMiddleware);

router.put('/:value', updateUserValid, function (req, res) {
    const updateUser = ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        password: req.body.password
    });
    let updatedUser = UserService.update(req.params.value, updateUser);
    if ( typeof updatedUser !== 'undefined') {
        res.status(200).send(updatedUser);
    } else {
        res.status(404).send({error: true,message: "Can't find user"});
    }
});

router.delete('/:value', function (req, res) {
    // TODO: check if id is exists
    let deleted = UserService.delete(req.params.value);
    res.status(200).send(deleted);
});

module.exports = router;