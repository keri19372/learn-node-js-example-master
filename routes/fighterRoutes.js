const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    let fighters = FighterService.getAllFighters();
    res.status(200).send(fighters);
}, responseMiddleware);

router.get('/:value', (req, res, next) => {
    let fighter = FighterService.find(req.params.value);
    if ( typeof fighter !== 'undefined') {
        res.status(200).send(fighter);
    } else {
        res.status(404).send({error: true,message: "Can't find fighter"});
    }
    res.status(200).send(fighter);
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    const newFighter = ({
        name: req.body.name,
        health: req.body.health,
        power: req.body.power,
        defence: req.body.defence
    });
    let fighter = FighterService.add(newFighter);
    res.status(200).send(fighter);
}, responseMiddleware);

router.put('/:value', updateFighterValid, function (req, res) {
    const update = ({
        name: req.body.name,
        health: req.body.health,
        power: req.body.power,
        defence: req.body.defence
    });
    let updatedFighter = FighterService.update(req.params.value, update);
    if ( typeof updatedFighter !== 'undefined') {
        res.status(200).send(updatedFighter);
    } else {
        res.status(404).send({error: true,message: "Can't find fighter"});
    }
});

router.delete('/:value', function (req, res) {
    // TODO: check if id is exists
    let deleted = FighterService.delete(req.params.value);
    res.status(200).send(deleted);
});

module.exports = router;