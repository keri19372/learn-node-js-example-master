const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    add(user) {
        return UserRepository.create(user);
    }

    getAllUsers() {
        return UserRepository.getAll();
    }

    find(id) {
        return UserRepository.findById(id);
    }

    delete(id) {
        UserRepository.delete(id);
    }

    update(id, user) {
        return UserRepository.update(id, user)
    }
}

module.exports = new UserService();