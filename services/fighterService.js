const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAllFighters() {
        return FighterRepository.getAll();
    }

    find(id) {
        return FighterRepository.findById(id);
    }

    add(fighter) {
        return FighterRepository.create(fighter);
    }

    update(id, fighter) {
        return FighterRepository.update(id, fighter)
    }

    delete(id) {
        return FighterRepository.delete(id)
    }
}

module.exports = new FighterService();