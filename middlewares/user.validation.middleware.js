const {user} = require('../models/user');
const { check, param, validationResult } = require('express-validator');

// TODO: refactor, remove duplicate
exports.updateUserValid = [
    check('email', 'Only gmail accepted').contains('gmail'),
    check('email').isEmail(),
    check('password', 'Password length min 3 symbols ').isLength({min: 3}),
    param('phoneNumber', 'phoneNumber: +380xxxxxxxxx').custom((value, { req }) => {
        const regex = /\+380\d{9}/g;
        return req.body.phoneNumber.match(regex);
    }),
    check('firstName', 'Necessary field').exists(),
    check('lastName', 'Necessary field').exists(),
    check('id').not().exists(),

    function(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            //return res.status(422).json({errors: errors.mapped()});
            return res.status(400).json({"error": true, "message": errors.mapped()})
        }
        next();
    },
];

exports.createUserValid = [
    check('email', 'Only gmail accepted').contains('gmail'),
    check('email').isEmail(),
    check('password', 'Password length min 3 symbols ').isLength({min: 3}),
    param('phoneNumber', 'phoneNumber: +380xxxxxxxxx').custom((value, { req }) => {
        const regex = /\+380\d{9}/g;
        return req.body.phoneNumber.match(regex);
    }),
    check('firstName', 'Necessary field').exists(),
    check('lastName', 'Necessary field').exists(),
    check('id').not().exists(),

    function(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({"error": true, "message": errors.mapped()})
        }
        next();
    },
];