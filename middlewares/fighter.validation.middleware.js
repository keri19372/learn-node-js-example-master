const { fighter } = require('../models/fighter');
const { check, validationResult } = require('express-validator');

// TODO: refactor, remove duplicate
exports.createFighterValid = [
    check('defense', 'Defense is between 1 and 10').matches(/^([1-9]|10)$/),
    check('power', 'power is between 1 and 100').matches(/^[1-9]$|^[1-9][0-9]$|^(100)$/),
    check('name', 'Necessary field').exists(),
    check('health', 'Necessary field').exists(),
    check('id').not().exists(),

    function(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            //return res.status(422).json({errors: errors.mapped()});
            return res.status(400).json({"error": true, "message": errors.mapped()})
        }
        next();
    },
];
exports.updateFighterValid = [
    check('defense', 'Defense is between 1 and 10').matches(/^([1-9]|10)$/),
    check('power', 'power is between 1 and 100').matches(/^[1-9]$|^[1-9][0-9]$|^(100)$/),
    check('name', 'Necessary field').exists(),
    check('health', 'Necessary field').exists(),
    check('id').not().exists(),

    function(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            //return res.status(422).json({errors: errors.mapped()});
            return res.status(400).json({"error": true, "message": errors.mapped()})
        }
        next();
    },
];